import React from "react";
import { useParams, Link, Redirect } from "react-router-dom";
import { getMember } from "../../components/aux";
const Company = (props) => {
  let { id } = useParams();
  let member = getMember(id, props.member);
  return member.type !== "pj" ? (
    <Redirect to="/" />
  ) : (
    <div>
      <h1>Detalhes da Empresa</h1>
      <Link to="/">Voltar</Link>
      <p>Nome da empresa: {member && member.name}</p>
    </div>
  );
};
export default Company;

import React from "react";
import { Link } from "react-router-dom";
const Links = (props) => {
  const link = props.member.map((member, key) => {
    return (
      <Link
        to={
          member.type === "pj"
            ? "company/" + member.id
            : "customer/" + member.id
        }
        key={key}
      >
        <p>{member.name}</p>
      </Link>
    );
  });
  return (
    <div>
      {link}
      <Link to="/form">New member</Link>
    </div>
  );
};
export default Links;

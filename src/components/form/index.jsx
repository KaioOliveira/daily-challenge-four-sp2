import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const validateMember = (setMembersError, memberName, memberType) => {
  if (memberName === "") {
    setMembersError("Member name cannot be blank");
    return;
  } else if (memberType === "") {
    setMembersError("Member Id cannot be blank");
    return;
  } else {
    setMembersError("");
    return true;
  }
};

const Form = (props) => {
  const [members, setMembers] = useState(props.member);
  const [membersError, setMembersError] = useState("");
  const [memberName, setMemberName] = useState("");
  const [memberType, setMemberType] = useState("");

  const submitMember = (e) => {
    e.preventDefault();
    const valid = validateMember(setMembersError, memberName, memberType);
    if (valid) {
      setMembers([
        ...members,
        {
          id: (Object.keys(members).length + 1).toString(),
          name: memberName,
          type: memberType,
        },
      ]);
    }
  };

  const setMemberNameValue = (e) => {
    setMemberName(e.target.value);
  };

  const setMemberTypeValue = (e) => {
    setMemberType(e.target.value);
  };

  useEffect(() => {
    props.setMembers(members);
  }, [members, props]);

  return (
    <div>
      <form onSubmit={submitMember}>
        <label>Create Member</label>
        <input
          type="text"
          onChange={(e) => setMemberNameValue(e)}
          value={memberName}
        ></input>
        <input
          type="text"
          onChange={(e) => setMemberTypeValue(e)}
          value={memberType}
        ></input>
        <button type="submit">New member</button>
      </form>
      {membersError && <p style={{ color: "red" }}>{membersError}</p>}
      <Link to="/">Voltar</Link>
    </div>
  );
};
export default Form;

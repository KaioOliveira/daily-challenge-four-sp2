import React, { useState } from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Customer from "./pages/customer/index";
import Company from "./pages/company/index";
import Home from "./pages/home/index";
import Form from "./components/form/index";
import { members } from "./components/aux";

function App() {
  const [thisMembers, setMembers] = useState(members);

  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route exact path="/">
            <Home member={thisMembers} />
          </Route>
          <Route path="/customer/:id">
            <Customer member={thisMembers} />
          </Route>
          <Route path="/company/:id">
            <Company member={thisMembers} />
          </Route>
          <Route path="/form">
            <Form member={thisMembers} setMembers={setMembers} />
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
